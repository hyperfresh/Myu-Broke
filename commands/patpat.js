// Aliasing?

module.exports.run = (bot, msg) => {
    msg.reply('**Deprecated** -> use `?pat` instead');
};

module.exports.help = {
    name: 'patpat',
    usage: "**Deprecated** -> use `?pat` instead",
    desc: ""
};